module Revrev where

-- reverse :: [a] -> [a]

prop_revrev :: Eq a => [a] -> Bool
prop_revrev xs = reverse (reverse xs) == xs

prop_revapp :: Eq a => [a] -> [a] -> Bool
prop_revapp xs ys = reverse (xs <> ys) == reverse ys <> reverse xs

                  
-- >>> prop_revapp [1,2,3] [4,5,6]
-- True

-- >>> [1,2,3] <> [4,5,6]    
-- [1,2,3,4,5,6]

-- >>> 1 : [4,5,6]
-- [1,4,5,6]

-- >>> collect (length [4,5,6])
-- Variable not in scope: collect :: Int -> f0

-- >>> :t [1,5,6]::[Int]
-- [1,5,6]::[Int] :: [Int]
