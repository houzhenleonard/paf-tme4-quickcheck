module Split where

split1 :: Char -> String -> [String]
split1 d [] = []
split1 d s = x : split d (drop 1 y) where (x,y) = span (/= d) s

split :: Char -> String -> [String]
split c s = case break (==c) s 
    of 
  (ls, "") -> [ls]
  (ls, x:rest) -> ls : split c rest



unsplit :: Char -> [String] -> String

unsplit ch [] =  ""
--unsplit ch ws = foldr (\a b-> a ++ if b=="" then b else ch : b) ws 
--unsplit ch xs = foldr (\a b-> a ++ (ch : b)) "" xs
unsplit ch (x:xs) =  x ++ go xs
  where 
    go [] = ""
    go (v:vs) = ch :(v ++ go vs)

prop_split_unsplit :: Char -> String -> Bool
prop_split_unsplit c str = unsplit c (split c str) == str



-- >>>unsplit 'b' ["a","d"]
-- "abd"

-- >>> split 'a' "a"
-- ["",""]


-- >>> unsplit 'a' (split 'a' "a")
-- "a"

